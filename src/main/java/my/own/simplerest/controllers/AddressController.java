package my.own.simplerest.controllers;

import my.own.simplerest.data.AddressRepository;
import my.own.simplerest.data.UserRepository;
import my.own.simplerest.data.entities.ShortAddress;
import my.own.simplerest.data.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by varty_000 on 09.03.2016.
 */

@Controller
@RequestMapping("/addresses")
public class AddressController {

    @Autowired
    AddressRepository aRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String getAddressesList(Model model) {
        Iterable<ShortAddress> addresses = aRepository.findAll();
        model.addAttribute("addresses", addresses);
        return "address_list";
    }

    @RequestMapping(value = "/new_address", method = RequestMethod.GET)
    public String newUser(Model model) {
        model.addAttribute("address", new ShortAddress());
        return "new_address";
    }

    @RequestMapping(value = "/{addressId}", method = RequestMethod.GET)
    public String getUser(@PathVariable("addressId") int id, Model model) {
        Optional<ShortAddress> address = aRepository.findById(id);
        if (address.isPresent()) {
            model.addAttribute("address", address.get());
            return "address_info";
        }
        return "address_not_find";
    }

}
