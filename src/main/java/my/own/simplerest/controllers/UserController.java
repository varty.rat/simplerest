package my.own.simplerest.controllers;

import my.own.simplerest.data.AddressRepository;
import my.own.simplerest.data.UserRepository;
import my.own.simplerest.data.entities.ShortAddress;
import my.own.simplerest.data.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by varty_000 on 09.03.2016.
 */

@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserRepository uRepository;

    @Autowired
    AddressRepository aRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String getUsersList(Model model) {
        Iterable<User> users = uRepository.findAll();
        model.addAttribute("userList", users);
        return "user_list";
    }

    @RequestMapping(value = "/new_user", method = RequestMethod.GET)
    public String newUser(Model model) {
        Iterable<ShortAddress> address = aRepository.findAll();
        model.addAttribute("user", new User());
        model.addAttribute("addresses", address);
        return "new_user";
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public String getUser(@PathVariable("userId") int id, Model model) {
        Optional<User> user = uRepository.findById(id);
        Iterable<ShortAddress> addresses = aRepository.findAll();
        if (user.isPresent()) {
            model.addAttribute("user", user.get());
            model.addAttribute("addresses", addresses);
            return "user_info";
        }
        return "user_not_find";
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addNewUser(@ModelAttribute User user, Model model) {
        uRepository.save(user);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateNewUser(@RequestBody User user) {
        uRepository.save(user);
    }
}
