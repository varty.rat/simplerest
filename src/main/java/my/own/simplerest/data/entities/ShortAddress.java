package my.own.simplerest.data.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by varty_000 on 08.03.2016.
 */

@Entity
@Table(name = "simple_address")
public class ShortAddress {

    @OneToMany(mappedBy = "address")
    private Set<User> residents;

    @Id
    @GeneratedValue
    private int id;

    private String city;

    private String street;

    @Column(name = "house")
    private String houseNumber;

    public ShortAddress() {
    }

    public ShortAddress(Set<User> residents, String city, String street, String houseNumber) {
        this.residents = residents;
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
    }

    public int getId() {
        return id;
    }

    public Set<User> getResidents() {
        return residents;
    }

    public void setResidents(Set<User> residents) {
        this.residents = residents;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }
}
