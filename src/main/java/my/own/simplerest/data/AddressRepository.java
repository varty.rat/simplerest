package my.own.simplerest.data;

import my.own.simplerest.data.entities.ShortAddress;
import my.own.simplerest.data.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by varty_000 on 08.03.2016.
 */
@Repository
public interface AddressRepository extends CrudRepository<ShortAddress, Integer> {
    Optional<ShortAddress> findById(int id);
}
