package my.own.simplerest.data;

import my.own.simplerest.data.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by varty_000 on 08.03.2016.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findById(int id);

    List<User> findByLastName(String lastName);

    List<User> findByFirstName(String firstName);
}
