package my.own.simplerest.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by varty_000 on 10.03.2016.
 */
@SpringBootApplication
@ComponentScan("my.own.simplerest")
@EntityScan("my.own.simplerest.data.entities")
@EnableJpaRepositories("my.own.simplerest.data")
public class SimpleRest {

    public static void main(String... args) {
        SpringApplication.run(SimpleRest.class, args);
    }
}
